package com.qms.games.superp;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector3f;
import com.jme3.system.AppSettings;
import com.simsilica.es.EntityData;
import com.simsilica.es.base.DefaultEntityData;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    private ScheduledExecutorService executor;
    private GameLogic gameLogic;
    private EntityData entityData;
    
    public static void main(String[] args) {
        Main app = new Main();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 768);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        this.entityData = new DefaultEntityData();
        this.getFlyByCamera().setEnabled(false);
        this.getCamera().setLocation(new Vector3f(0,3f,0));
        this.getCamera().lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
        
        this.executor = Executors.newSingleThreadScheduledExecutor();
        this.executor.scheduleAtFixedRate(new GameLogic(this.entityData, this), 
                                            0, 20, TimeUnit.MILLISECONDS);
    }

    @Override
    public void destroy() {
        super.destroy();
        this.entityData.close();
        this.executor.shutdown();
    }
}
