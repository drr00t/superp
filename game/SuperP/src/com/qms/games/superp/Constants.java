/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp;

/**
 * Todas as constantes do jogo serão estão mapeadas aqui.
 * @author adriano
 */
public final class Constants {
    /**
     * relação dos mapas do jogo
     */
    public final class Maps{
        
        /**
         * primeira versão do mapa da área de jogo.
         */
        public static final String MainScene = "Models/modelo/modelo.j3o";
    }
    
    public final class Players{
        
        /**
         * primeira versão do mapa da área de jogo.
         */
        public static final String PlayerLeft = "Player_Left";
        
        public static final String PlayerRight = "Player_Right";
    }
    
    public final class GameElements{
        
        /**
         * game ball
         */
        public static final String Ball = "ball";
    }
}
