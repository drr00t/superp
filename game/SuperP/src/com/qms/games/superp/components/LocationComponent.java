/* Copyright (C) YoYoDyne Systems, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.qms.games.superp.components;

import com.jme3.math.Vector3f;
import com.simsilica.es.EntityComponent;

/**
 *
 * @author adriano
 */
public class LocationComponent implements  EntityComponent{

    public LocationComponent(Vector3f position, boolean up) {
        this.position = position;
        this.up = up;
    }

    public Vector3f getPosition() {
        return position;
    }

    public boolean isUp() {
        return up;
    }
    
    private Vector3f position;
    private boolean up;

}
