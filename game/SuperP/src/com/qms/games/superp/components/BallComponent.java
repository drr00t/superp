/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.simsilica.es.EntityComponent;

/**
 * ball to play the game
 * @author adriano
 */
public class BallComponent implements EntityComponent{
    
    private boolean directionUp;
    private boolean directionLeft;
    private float speedVertical;
    private float speedHorizontal;

    public BallComponent(boolean directionUp, boolean directionLeft, 
                        float speedVertical, float speedHorizontal) {
        this.directionUp = directionUp;
        this.directionLeft = directionLeft;
        this.speedVertical = speedVertical;
        this.speedHorizontal = speedHorizontal;
    }

    public float getSpeedHorizontal() {
        return speedHorizontal;
    }

    public void setSpeedHorizontal(float speedHorizontal) {
        this.speedHorizontal = speedHorizontal;
    }

    
    public float getSpeedVertical() {
        return speedVertical;
    }

    public void setSpeedVertical(float speedVertical) {
        this.speedVertical = speedVertical;
    }

    public boolean isDirectionUp() {
        return directionUp;
    }

    public void setDirectionUp(boolean directionUp) {
        this.directionUp = directionUp;
    }

    public boolean isDirectionLeft() {
        return directionLeft;
    }

    public void setDirectionLeft(boolean directionLeft) {
        this.directionLeft = directionLeft;
    }
}
