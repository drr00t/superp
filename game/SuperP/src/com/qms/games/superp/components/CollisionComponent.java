/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.simsilica.es.EntityComponent;

/**
 * Identifica tudo o que pode colidir com a bola na cena.
 * @author adriano
 */
public class CollisionComponent implements EntityComponent {
    private String name;
    private String effectName;

    public CollisionComponent(String name, String effectName) {
        this.name = name;
        this.effectName = effectName;
    }

    public String getEffectName() {
        return effectName;
    }

    public String getName() {
        return name;
    }

}
