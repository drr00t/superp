/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.simsilica.es.EntityComponent;

/**
 * Diferencia um jogador humano do jogador controlado pela IA.
 * @author adriano
 */
public class PlayerComponent implements EntityComponent {
    private boolean human;

    public PlayerComponent(boolean human) {
        this.human = human;
    }

    public boolean isHuman() {
        return human;
    }
}
