/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.simsilica.es.EntityComponent;

/**
 *
 * @author adriano
 */
public class RenderComponent  implements  EntityComponent{
    private String modelFile;

    public RenderComponent(String modelFile) {
        this.modelFile = modelFile;
    }

    public String getModelFile() {
        return modelFile;
    }
}
