/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.simsilica.es.EntityComponent;

/**
 * ball to play the game
 * @author adriano
 */
public class BallHitComponent implements EntityComponent{
    
    private int index;
    private float speedVertical;
    private float speedHorizontal;

    public BallHitComponent(int index, float speedVertical, float speedHorizontal) {
        this.index = index;
        this.speedVertical = speedVertical;
        this.speedHorizontal = speedHorizontal;
    }

    public int getIndex() {
        return index;
    }

    public float getSpeedHorizontal() {
        return speedHorizontal;
    }

    public float getSpeedVertical() {
        return speedVertical;
    }
}
