/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.components;

import com.jme3.math.Vector3f;
import com.simsilica.es.EntityComponent;

/**
 * Identifica tudo o que pode colidir com a bola na cena.
 * @author adriano
 */
public class PlayerPartThreeCollisionComponent implements EntityComponent {
    private String name;
    private Vector3f direction;

    public PlayerPartThreeCollisionComponent(String name, Vector3f direction) {
        this.name = name;
        this.direction = direction;
    }

    public String getName() {
        return name;
    }

    public Vector3f getDirection() {
        return direction;
}

}
