/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.factories;

import com.jme3.math.Vector3f;
import com.qms.games.superp.GameLogic;
import com.qms.games.superp.components.BallComponent;
import com.qms.games.superp.components.BallHitComponent;
import com.qms.games.superp.components.LocationComponent;
import com.qms.games.superp.components.PlayerComponent;
import com.qms.games.superp.components.PlayerPartFiveCollisionComponent;
import com.qms.games.superp.components.PlayerPartFourCollisionComponent;
import com.qms.games.superp.components.PlayerPartOneCollisionComponent;
import com.qms.games.superp.components.PlayerPartThreeCollisionComponent;
import com.qms.games.superp.components.PlayerPartTwoCollisionComponent;
import com.qms.games.superp.components.RenderComponent;
import com.simsilica.es.EntityId;
import com.simsilica.es.Name;

/**
 *
 * @author adriano
 */
public class EntityFactory {

    private GameLogic gameLogic;
    
    public EntityFactory(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }
    
    /**
     * create a new player for the game
     * @param position initial player position
     * @param playerName player name
     * @param human  true if the player is human, false to computer
     * 
     */
    public EntityId createPlayer(Vector3f position, String playerName, 
                            boolean human){
        
        return gameLogic.createEntity(new LocationComponent(position, true),
                new PlayerComponent(human),
                new Name(playerName),
                new RenderComponent(playerName));
    }
    
    /**
     * crate ball to play the game
     * @param position initial position
     * @param ballName 
     */
    public EntityId createBall(Vector3f position, String ballName, 
                            boolean directionUp, boolean directionLeft, 
                            float speedHorizontal, float speedVertical){
        return gameLogic.createEntity(
                new LocationComponent(position, true),
                new BallComponent(directionUp, directionLeft, 
                                    speedVertical, speedHorizontal),
                new Name(ballName),
                new RenderComponent(ballName));
    }
    
    public EntityId createPlayerPartOne(String name, Vector3f direction){
        return gameLogic.createEntity(
            new PlayerPartOneCollisionComponent(name, direction)
        );
    }
    
    public EntityId createPlayerPartTwo(String name, Vector3f direction){
        return gameLogic.createEntity(
            new PlayerPartTwoCollisionComponent(name, direction)
        );
    }
    
    public EntityId createPlayerPartThree(String name, Vector3f direction){
        return gameLogic.createEntity(
            new PlayerPartThreeCollisionComponent(name, direction)
        );
    }
    
    public EntityId createPlayerPartFour(String name, Vector3f direction){
        return gameLogic.createEntity(
            new PlayerPartFourCollisionComponent(name, direction)
        );
    }
    
    public EntityId createPlayerPartFive(String name, Vector3f direction){
        return gameLogic.createEntity(
            new PlayerPartFiveCollisionComponent(name, direction)
        );
    }
}
