/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.scene.Node;
import com.qms.games.superp.appstates.AIInputAppState;
import com.qms.games.superp.appstates.InGameAppState;
import com.qms.games.superp.appstates.PlayerInputAppState;
import com.qms.games.superp.appstates.RenderAppState;
import com.simsilica.es.ComponentFilter;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityComponent;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 *
 * @author adriano
 * 
 * Thread para execução da lógica do jogo.
 */
public class GameLogic implements Runnable {
    private static final Logger LOG = Logger.getLogger(GameLogic.class.getName());

    private final float DEFAULT_TPF = 0.02f;
    
    private float frameTime = 0f;
        
    private EntityData entityData;

    private AppStateManager gameLogicProcessors;
    
    private PhysicsSpace physics;

    private SimpleApplication simpleApp;

    public GameLogic(EntityData entityData, SimpleApplication simpleApp) {
        this.entityData = entityData;
        this.gameLogicProcessors = new AppStateManager(simpleApp);
        this.simpleApp = simpleApp;
        
        this.gameLogicProcessors.attach(new InGameAppState(this));
        this.gameLogicProcessors.attach(new PlayerInputAppState(this));
        this.gameLogicProcessors.attach(new AIInputAppState(this));
        this.gameLogicProcessors.attach(new RenderAppState(this));

    }

    /**
     * get physics space
     * @return 
     */
    public PhysicsSpace getPhysics() {
        return physics;
    }
            
    /**
     * access to SimpleApplication 
     * @return 
     */
    public SimpleApplication getSimpleApp() {
        return simpleApp;
    }

    
        
    /**
     * cria uma nova entidade com o conjunto de componentes 
     * @param components
     * @return 
     */
    public EntityId createEntity(EntityComponent ... components){
        EntityId entity = this.entityData.createEntity();
        this.entityData.setComponents(entity, components);
        return entity;
    }

    /**
     * atualiza um ou mais componentes de uma entidade do jogo
     * @param entityId
     * @param components 
     */
    public void updateEntity(EntityId entityId, EntityComponent...components){
        this.entityData.setComponents(entityId, components);
    }
    
    /**
     * remove uma entitidade a partir do seu id
     * @param entityId 
     */
    public void removeEntity(EntityId entityId){
        this.entityData.removeEntity(entityId);
    }
    
    /**
     * remove um componente de uma entidade
     * @param entityId
     * @param components 
     */
    public void removeComponent(EntityId entityId, Class<?> components){
        this.entityData.removeComponent(entityId, components);
    }
    
    /**
     * consulta uma lista de entidades a partir de uma lista de componentes
     * @param components
     * @return lista de entidade que possuem os componentes
     */
    public EntitySet queryEntities(Class<?> ... components){
        return this.entityData.getEntities(components);
    }
    
    /**
     * consulta uma lista de entidades que atendam ao filtro e possuam os 
     * componentes informados
     * @param filter 
     * @param components
     * @return lista de entidades e os valores dos componente especificados
     */
    public EntitySet queryEntities(ComponentFilter filter, Class<?> ... components){
        return this.entityData.getEntities(filter, components);
    }
    
    /**
     * retorna os valores do componentes informados a paritr dos componentes
     * e o id da entidade
     * @param entityId
     * @param components
     * @return 
     */
    public Entity getEntityById(EntityId entityId, Class<?>...components){
        return this.entityData.getEntity(entityId, components);
    }
    
    /**
     * agenda um nó para ser incluído na cena
     */
    public void attachChild(final Node node) {

        this.simpleApp.enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                simpleApp.getRootNode().attachChild(node);
                return null;
            }
        });
    }
    /**
     * retorna o AppStateManager com os processadores da 
     * lógica do jogo registrados.
     * @return 
     */
    public AppStateManager getStateManager() {
        return gameLogicProcessors;
    }

    /**
     * query a registred logic processor
     * @param <T>
     * @param processor
     * @return 
     */
    public <T extends AppState> T getGameLogicProcessor(Class<T> processor){
        return this.gameLogicProcessors.getState(processor);
    }

    /*
     * Loop de execução do jogo sempre que houve diferença de tempo entre 
     * os frames.
     */
    public void run() {

        float time = this.simpleApp.getTimer().getTimeInSeconds();
        float delta = time - this.frameTime;
        this.frameTime = time;

//        LOG.info(String.format("update game logic thread: %f",delta));
                
        if (delta == 0) {
            return;
        }

        if (delta > this.DEFAULT_TPF) {
            delta = this.DEFAULT_TPF;
        }
        
        this.gameLogicProcessors.update(delta);
        
//        LOG.info(String.format("game step logic update thread: %f",delta));

    }
}
