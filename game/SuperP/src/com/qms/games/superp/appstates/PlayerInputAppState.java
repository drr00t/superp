/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.qms.games.superp.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.qms.games.superp.GameLogic;
import com.qms.games.superp.components.LocationComponent;
import com.qms.games.superp.components.PlayerComponent;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Filters;
import java.util.logging.Logger;

/**
 *
 * @author adriano
 */
public class PlayerInputAppState extends AbstractAppState implements AnalogListener {

    private static final Logger LOG =
            Logger.getLogger(PlayerInputAppState.class.getName());
    private GameLogic gameLogic;
    private EntityData entityData;
    private EntityId entityId;
    private String moveUp = "MoveUp";
    private String moveDown = "MoveDown";
    private float Z_UPPER_LIMIT = 0.55f;
    private float Z_BOTTON_LIMIT = -0.94f;

    public PlayerInputAppState(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.gameLogic.getSimpleApp().getInputManager()
                .addMapping(moveUp, new KeyTrigger(KeyInput.KEY_UP));
        this.gameLogic.getSimpleApp().getInputManager()
                .addMapping(moveDown, new KeyTrigger(KeyInput.KEY_DOWN));

        this.gameLogic.getSimpleApp().getInputManager()
                .addListener(this, moveDown, moveUp);

        EntitySet entitySet = this.gameLogic.queryEntities(Filters
                .fieldEquals(PlayerComponent.class, "human", true),
                LocationComponent.class, PlayerComponent.class);

        for (Entity entity : entitySet) {
            this.entityId = entity.getId();
            break;
        }
    }

    @Override
    public void cleanup() {
        this.gameLogic.getSimpleApp().getInputManager().removeListener(this);
        this.gameLogic.getSimpleApp().getInputManager().deleteMapping(moveUp);
        this.gameLogic.getSimpleApp().getInputManager().deleteMapping(moveDown);
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
        Entity entity = this.gameLogic.getEntityById(entityId,
                LocationComponent.class, PlayerComponent.class);
        float speed = 0.002f;
        LocationComponent lc = entity.get(LocationComponent.class);
        Vector3f movement = lc.getPosition().clone();

        if (name.equals(moveDown)) {

            movement.setZ(movement.z - speed);
            if (movement.z > Z_BOTTON_LIMIT) {

                entity.set(new LocationComponent(movement, false));
            }

//            LOG.info(String.format("move down: %s", movement.toString()));

        } else if (name.equals(moveUp)) {

            movement.setZ(movement.z + speed);
            if (movement.z < Z_UPPER_LIMIT) {
                entity.set(new LocationComponent(movement, true));
            }
//            LOG.info(String.format("move down: %s", movement.toString()));
        }
    }
}
