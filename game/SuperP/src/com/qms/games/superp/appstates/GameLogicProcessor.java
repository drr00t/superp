/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.qms.games.superp.GameLogic;
import com.simsilica.es.Entity;
import com.simsilica.es.EntitySet;
import java.util.Set;

/**
 * must implementa part of game logic 
 * @author adriano
 */
public abstract class GameLogicProcessor extends AbstractAppState {

    protected GameLogic gameLogic;
    protected EntitySet entitySet;

    public GameLogicProcessor(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

    }
    
    @Override
    public void update(float tpf) {
        if(this.entitySet.applyChanges()){
            
            processRemovedEntities(this.entitySet.getRemovedEntities());
            processAddedEntities(this.entitySet.getAddedEntities());
            processChangedEntities(this.entitySet.getChangedEntities());
            
        }
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
        entitySet.release();
    }
    
    protected abstract void processAddedEntity(Entity entity);
    
    protected abstract void processChangedEntity(Entity entity);
    
    protected abstract void processRemovedEntity(Entity entity);
    
    /**
     * process all entities added on this frame
     * @param entities 
     */
    private void processAddedEntities(Set<Entity> entities){
        for(Entity e :entities){
            processAddedEntity(e);
        }        
    }
    
    /**
     * process all entities that had change in any of filter components
     * @param entities 
     */
    private void processChangedEntities(Set<Entity> entities){
        for(Entity e :entities){
            processChangedEntity(e);
        }
    }
    
    /**
     * process all entities that had removed from this filter
     * @param entities 
     */
    private void processRemovedEntities(Set<Entity> entities){
        for(Entity e :entities){
            processRemovedEntity(e);
        }        
    }   
}
