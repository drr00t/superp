/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.qms.games.superp.Constants;
import com.qms.games.superp.GameLogic;
import com.qms.games.superp.components.BallComponent;
import com.qms.games.superp.components.LocationComponent;
import com.qms.games.superp.factories.EntityFactory;
import com.qms.games.superp.factories.ModelFactory;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityId;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 *
 * @author adriano
 */
public class InGameAppState extends AbstractAppState {

    private static final Logger LOG = Logger.getLogger(InGameAppState.class.getName());
    private Node gameWorld;
    private GameLogic gameLogic;
    private EntityFactory entityFactory;
    private ModelFactory modelFactory;
    private PhysicsSpace physicsSpace;
    private float speedX = 0.02f;
    private float speedZ = 0.02f;
    Node ball;
    Node right;
    Node left;
    private Vector3f ballPosition = Vector3f.ZERO;
    private Quaternion rotateL;
    private float Z_UPPER_LIMIT = 0.94f;
    private float Z_BOTTON_LIMIT = -0.94f;
    private float X_LEFT_LIMIT = 1.4f;
    private float X_RIGHT_LIMIT = -1.4f;
    private boolean directionUp = true;
    private boolean directionLeft = true;
    private EntityId ballEntityId;
    private EntityId leftEntityId;
    private EntityId rightEntityId;
    private HashMap<EntityId, Spatial> playersParts;

    public InGameAppState(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    /**
     * retorna o nó raiz da cena do jogo.
     *
     * @return
     */
    public Node getGameWorld() {
        return gameWorld;
    }

    /**
     * inicializa o AppState quando ele é atachado ao AppStateManager
     *
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.gameWorld = new Node();
        ((SimpleApplication) app).getRootNode().attachChild(gameWorld);

        this.entityFactory = new EntityFactory(this.gameLogic);
        this.modelFactory = new ModelFactory(this.gameLogic);

        this.playersParts = new LinkedHashMap<EntityId, Spatial>(10);

        Node scene = (Node) app.getAssetManager().
                loadModel(Constants.Maps.MainScene);
        this.attachChild(scene);

        this.left = (Node) scene.getChild(Constants.Players.PlayerLeft);
        this.leftEntityId = this.entityFactory.createPlayer(left.getLocalTranslation(),
                left.getName(), true);

        this.loadPlayersParts(this.left);


        this.right = (Node) scene.getChild(Constants.Players.PlayerRight);
        this.rightEntityId = this.entityFactory.createPlayer(right.getLocalTranslation(),
                right.getName(), false);

        this.loadPlayersParts(this.right);

        this.ball = (Node) scene.getChild(Constants.GameElements.Ball);
        this.ballEntityId = this.entityFactory.createBall(ball.getLocalTranslation(),
                ball.getName(), true, false, -speedX, speedZ);

    }

    /**
     * agenda um nó para ser incluído na cena
     */
    public void attachChild(final Node node) {

        this.gameLogic.getSimpleApp().enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                gameWorld.attachChild(node);
                return null;
            }
        });
    }

    /**
     * Agenda um nós para remoção da cena
     *
     * @param node
     */
    public void detachChild(final Node node) {

        this.gameLogic.getSimpleApp().enqueue(new Callable<Void>() {
            public Void call() throws Exception {

                gameWorld.detachChild(node);
//                gameLogic.getPhysics().remove(node.getControl(RigidBodyControl.class));

                LOG.info(String.format("loading new node: %s, position: %s",
                        node.getName(), node.getLocalTranslation().toString()));

                return null;
            }
        });
    }

    @Override
    public void update(float tpf) {

//        if(left.getWorldBound().contains(ballPosition)){
//            updateBallVerticalLimits(); 
        updateBallPosition();
////            LOG.info(String.format("player hit the ball: %s",left.getName()));
//        }else{
//            updateBallPosition(false);
//        }

//        LOG.info(String.format("update game loop time thread: %f",tpf));
    }

    /**
     * faz a limpeza do conteúdo da cena quando o AppState for removido o loop
     * de processamento
     */
    @Override
    public void cleanup() {
        super.cleanup();

        this.gameLogic.getSimpleApp().enqueue(new Callable<Void>() {
            public Void call() throws Exception {
                gameWorld.detachAllChildren();
                LOG.info("scene cleanup");

                return null;
            }
        });
    }

    /**
     * natural ball movement
     *
     * @param positionHorizontal
     * @param positionVertical
     */
    private void updateBallPosition() {
        Entity entity = this.gameLogic.getEntityById(this.ballEntityId,
                BallComponent.class, LocationComponent.class);

        BallComponent bc = entity.get(BallComponent.class);
        LocationComponent lc = entity.get(LocationComponent.class);

        if (((lc.getPosition().getZ() <= Z_BOTTON_LIMIT))
                || (lc.getPosition().getZ() >= Z_UPPER_LIMIT)) {
            directionUp = !directionUp;
        }


        Iterator<Map.Entry<EntityId, Spatial>> itr = this.playersParts.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry<EntityId, Spatial> part = itr.next();

            if (part.getValue().getWorldBound().contains(ballPosition)) {

                directionLeft = !directionLeft;

                LOG.info(String.format("player part %s hit the ball", part.getValue().getName()));
            }
        }

//        if ((lc.getPosition().getX() <= X_RIGHT_LIMIT)
//                || (lc.getPosition().getX() >= X_LEFT_LIMIT)) {
//            directionLeft = !directionLeft;
//        }

        if (directionUp) {
            this.ballPosition.setZ(lc.getPosition().getZ()
                    + bc.getSpeedVertical());
        } else {
            this.ballPosition.setZ(lc.getPosition().getZ()
                    - bc.getSpeedVertical());
        }

        if (directionLeft) {
            this.ballPosition.setX(lc.getPosition().getX()
                    + bc.getSpeedHorizontal());
        } else {
            this.ballPosition.setX(lc.getPosition().getX()
                    - bc.getSpeedHorizontal());
        }
//
//        entity.set(new BallComponent(directionUp, directionLeft, 
//                    bc.getSpeedVertical(), bc.getSpeedHorizontal()));

        entity.set(new LocationComponent(this.ballPosition.clone(), directionUp));
    }

    /**
     * collidable player parts list afeting tha ball movement
     *
     * @param player
     */
    private void loadPlayersParts(Node player) {

        EntityId id0 = this.entityFactory.createPlayerPartOne(player.getChild(0).getName(), new Vector3f(speedX, 0, speedZ + 0.01f));
        this.playersParts.put(id0, player.getChild(0));

        EntityId id1 = this.entityFactory.createPlayerPartTwo(player.getChild(1).getName(), new Vector3f(speedX, 0, speedZ + 0.02f));
        this.playersParts.put(id1, player.getChild(1));

        EntityId id2 = this.entityFactory.createPlayerPartTwo(player.getChild(2).getName(), new Vector3f(speedX, 0, speedZ + 0.00f));
        this.playersParts.put(id2, player.getChild(2));

        EntityId id3 = this.entityFactory.createPlayerPartTwo(player.getChild(3).getName(), new Vector3f(speedX, 0, speedZ + 0.02f));
        this.playersParts.put(id3, player.getChild(3));

        EntityId id4 = this.entityFactory.createPlayerPartTwo(player.getChild(4).getName(), new Vector3f(speedX, 0, speedZ + 0.01f));
        this.playersParts.put(id4, player.getChild(4));

    }
}
