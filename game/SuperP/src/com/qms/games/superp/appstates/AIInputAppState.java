/* 
 * The MIT License
 *
 * Copyright 2013 adriano.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.qms.games.superp.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.qms.games.superp.GameLogic;
import com.qms.games.superp.components.BallComponent;
import com.qms.games.superp.components.LocationComponent;
import com.qms.games.superp.components.PlayerComponent;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Filters;
import java.util.logging.Logger;

/**
 *
 * @author adriano
 */
public class AIInputAppState extends GameLogicProcessor {

    private static final Logger LOG =
            Logger.getLogger(AIInputAppState.class.getName());

    private EntityId entityId;
    
    private Entity playerAI;

    private float Z_UPPER_LIMIT = 0.55f;
    private float Z_BOTTON_LIMIT = -0.94f;

    public AIInputAppState(GameLogic gameLogic) {
        super(gameLogic);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        EntitySet entitySet = this.gameLogic.queryEntities(Filters
                .fieldEquals(PlayerComponent.class, "human", false),
                LocationComponent.class, PlayerComponent.class);

        for (Entity entity : entitySet) {
            this.entityId = entity.getId();
            this.playerAI = entity;
            break;
        }
        
        this.entitySet = this.gameLogic.queryEntities(
                LocationComponent.class, BallComponent.class);
    }

    @Override
    protected void processAddedEntity(Entity entity) {

    }

    @Override
    protected void processChangedEntity(Entity entity) {
        updatePlayerPosition(entity);
    }

    @Override
    protected void processRemovedEntity(Entity entity) {

    }
    
    private void updatePlayerPosition(Entity entity){
        float speed = 0.02f;
        LocationComponent lcBall = entity.get(LocationComponent.class);
        LocationComponent lcPlayer = playerAI.get(LocationComponent.class);
        Vector3f movement = lcPlayer.getPosition().clone();

        if( Math.abs(lcBall.getPosition().getZ() - movement.z) >= 0.2f){
            return;
        }
                
        if (!lcBall.isUp()) {
//            if((lcBall.getPosition().getZ() - movement.z) >= 0.05f){
                
                LOG.info(String.format("move down ball distance: %f", 
                                    lcBall.getPosition().getZ() - movement.z));
                
                movement.setZ(movement.z - speed);
                if (movement.z > Z_BOTTON_LIMIT) {

                    super.gameLogic.updateEntity(entityId, 
                            new LocationComponent(movement, lcBall.isUp()));
                }
//            }

//            LOG.info(String.format("move down: %s", movement.toString()));

        } else if (lcBall.isUp()) {

//            if((lcBall.getPosition().getZ() + movement.z) <= 0.05f){
                
                LOG.info(String.format("move up ball distance: %f", 
                                    lcBall.getPosition().getZ() + movement.z));
                
                movement.setZ(movement.z + speed);
                if (movement.z < Z_UPPER_LIMIT) {
                    super.gameLogic.updateEntity(entityId, 
                            new LocationComponent(movement, lcBall.isUp()));
                }
//            }
//            LOG.info(String.format("move down: %s", movement.toString()));
        }
    }
}
