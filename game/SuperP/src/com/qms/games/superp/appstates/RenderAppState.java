/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qms.games.superp.appstates;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.qms.games.superp.GameLogic;
import com.qms.games.superp.components.LocationComponent;
import com.qms.games.superp.components.RenderComponent;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityId;
import com.simsilica.es.EntitySet;
import com.simsilica.es.Name;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 *
 * @author adriano
 */
public class RenderAppState extends GameLogicProcessor {

    private InGameAppState inGame;
//    private ModelFactory factoryModels;
    private HashMap<EntityId, Spatial> spatials;

    public RenderAppState(GameLogic gameLogic) {
        super(gameLogic);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.entitySet = this.gameLogic.queryEntities(
                LocationComponent.class, RenderComponent.class, Name.class);

        this.inGame = this.gameLogic.getGameLogicProcessor(InGameAppState.class);
        this.spatials = new HashMap<EntityId, Spatial>();
//        this.factoryModels = new ModelFactory(this.simpleApp);
    }

    @Override
    public void cleanup() {
        super.cleanup();
        this.spatials.clear();
    }

    @Override
    protected void processAddedEntity(Entity entity) {
        updateEntity(entity);
    }

    @Override
    protected void processChangedEntity(Entity entity) {
        updateEntity(entity);
    }

    @Override
    protected void processRemovedEntity(Entity entity) {
        if (this.spatials.containsKey(entity.getId())) {
            Node node = (Node) this.spatials.get(entity.getId());
            this.spatials.remove(entity.getId());
            this.inGame.detachChild(node);
        }
    }
    

    /**
     * adiciona ou atualiza a apresentação visual de uma entidade que sofreu
     * alterações no Location ou Render
     *
     * @param entity
     */
    private void updateEntity(Entity entity) {
        final RenderComponent rc = entity.get(RenderComponent.class);
        final LocationComponent lc = entity.get(LocationComponent.class);
        final Name nc = entity.get(Name.class);
        final Spatial spatial;

        if (this.spatials.containsKey(entity.getId())) {

            spatial = this.spatials.get(entity.getId());

        } else {

            spatial = this.inGame.getGameWorld().getChild(nc.getName());
            this.spatials.put(entity.getId(), spatial);
        }

        this.gameLogic.getSimpleApp().enqueue(new Callable<Void>() {
            public Void call() throws Exception {

                spatial.setLocalTranslation(lc.getPosition());

                return null;
            }
        });

    }
    
}
