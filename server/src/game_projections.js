var blanket = require("blanket");
var EventEmitter = require('events').EventEmitter;
var crypto = require('crypto');
var sha1 = crypto.createHash('sha1');

var Projections = function(){
	EventEmitter.call(this);
};

Projections.prototype.getPlayersAvailable = function(args){
	this.emit('players:available',{'data':[], count:0});
};

module.exports = Projections();
