module.exports = function(grunt){
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-blanket');	

	grunt.initConfig({
  		jshint: {
    		all: ['Gruntfile.js', 'src/**/*.js', 'tests/**/*.js']
  		},
		clean: {
			coverage: {
				src: ['coverage/src/','coverage/tests/']
			}
		},
		copy: {
			coverage: {
				src: ['tests/**/*.js','src/**/*.js'],
				dest: 'coverage/'
			}
		},
		blanket: {
			coverage: {
				src: ['src/'],
				dest: 'coverage/src/'
			}
		},
    	mochaTest: {
			test: {
				options: {
					reporter: 'spec'
				},
				src: ['tests/**/*.js']
			}
		},
		coverage: {
			options: {
				reporter: 'html-cov',
				//quiet: true,
				captureFile: 'coverage.html'
			},
			src: ['coverage/tests/**/*.js']
		}
	});

	grunt.registerTask("tests-cover",['clean','blanket','copy','mochaTest']);
	grunt.registerTask("tests",['clean','copy','mochaTest']);
	grunt.registerTask("check-code", ['jshint']);
	grunt.registerTask("run-all",['clean','copy','jshint','mochaTest']);
};
