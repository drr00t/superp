var blanket = require("blanket");
var should = require('should');
var commands = require('../src/game_commands');

describe('GameCommands',function(){

	describe('RegisterPlayer', function(){

		it('load commands', function(){
			should(commands).have.property('registerPlayer');
		});

		it('player registred', function(){
			var result = commands.registerPlayer({'player':'player 1'});
			should(result).have.property('token');
		});
	});
});
